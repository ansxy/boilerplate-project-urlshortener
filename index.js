require("dotenv").config();
const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
const database = require("./database");
const bodyParser = require("body-parser");
const Url = require("./urlShort");
const isValidDomain = require("is-valid-domain");
const port = process.env.PORT || 3000;

app.use(cors());

app.use("/public", express.static(`${process.cwd()}/public`));

app.get("/", function (req, res) {
  res.sendFile(process.cwd() + "/views/index.html");
});

// Your first API endpoint
app.get("/api/hello", function (req, res) {
  res.json({ greeting: "hello API" });
});

app.listen(port, function () {
  console.log(`Listening on port ${port}`);
});

//Validate URL
function isValidURL(string) {
  var res = string.match(
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
  );
  return res !== null;
}

app.post(
  "/api/shorturl",
  bodyParser.urlencoded({ extended: true }),
  (req, res) => {
    let longUrl = req.body.url;
    let srtUrl = 0;
    if (isValidURL(longUrl) === true) {
      mongoose.connection
        .collection("urls")
        .countDocuments({})
        .then((alerts) =>
          mongoose.connection.collection("urls").insert({
            short_url: alerts + 1,
            original_url: longUrl,
          })
        );
      Url.countDocuments({}).then((count) => {
        res.send({ original_url: longUrl, short_url: count + 1 });
      });
    } else {
      res.send({ error: "Invalid url" });
    }
  }
);
app.get("/api/shorturl/:count", (req, res) => {
  req.url = req.params.count;
  Url.find({}, (err, url) => {
    const id = url[req.url - 1]["_id"];
    Url.findById(id, (err, url) => {
      const temp = url.toJSON();
      res.redirect(temp["original_url"]);
    });
  });
});
