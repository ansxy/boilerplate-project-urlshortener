var mongoose = require("mongoose");

let urlScheme = new mongoose.Schema({
  //SHORT URL
  short_url: {
    type: String,
    required: true,
  },
  original_url: {
    type: String,
  },
});

module.exports = mongoose.model("Url", urlScheme);
